package org.boosey.parking.space;

import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;
import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;
import lombok.ToString;

@ToString
@MongoEntity(collection = "space")
@ApplicationScoped
public class Space extends PanacheMongoEntity {
    public String number;
    public String ownerId;
    public String residentId;
    public Boolean residentApprovedByOwner;

    public static Space findByNumber(String number){
        return find("number", number).firstResult();
    }

    public static Optional<Space> findByNumberOptional(String number){
        return Optional.ofNullable(findByNumber(number));
    }

}