package org.boosey.parking.space;

import java.util.List;
import java.util.Optional;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;
import org.boosey.parking.servicebase.ServiceBase;
import org.boosey.parking.events.OwnerCreatedEvent;
import org.boosey.parking.events.OwnerDeletedEvent;
import org.boosey.parking.events.SpaceCreatedEvent;
import org.boosey.parking.events.SpaceDeletedEvent;
import org.boosey.parking.events.SpaceOwnerRemovedEvent;
import org.boosey.parking.events.SpaceOwnerSetEvent;
import org.boosey.parking.events.SpaceResidentApprovalRemovedEvent;
import org.boosey.parking.events.SpaceResidentRemovedEvent;

// @Data
@Slf4j
@ApplicationScoped
class SpaceService extends ServiceBase {

    // @EVENT
    @PostConstruct
    public void init() {
        this.createSubscription(new OwnerCreatedEvent(), (e)->handleOwnerCreated((OwnerCreatedEvent)e));
        this.createSubscription(new OwnerDeletedEvent(), (e)->handleOwnerDeleted((OwnerDeletedEvent)e));
    }

    // @EVENT
    @PreDestroy
    public void destroy() {
        destroyAllSubscriptions();
    }

    public class SpaceExistsException extends RuntimeException {
        static final long serialVersionUID = 1L;
        public SpaceExistsException() {
            super("Space Already Exists");
        }
    }

    // @QUERY
    public class SpaceDoesNotExistException extends RuntimeException {
        static final long serialVersionUID = 1L;
        public SpaceDoesNotExistException() {
            super("Space doesn't exist");
        }
    }
    
    // @QUERY
    public List<Space> listSpaces() {
        return Space.listAll();
    } 

    // @QUERY
    public Space getSpaceById(@NonNull String id) throws SpaceDoesNotExistException {
        Optional<Space> o = Space.findByIdOptional(new ObjectId(id));
        o.orElseThrow(SpaceDoesNotExistException::new);        
        return o.get();
    }

    // @COMMAND
    public long deleteAllSpaces() {
        long count = Space.count();
        Space.<Space>streamAll().forEach(o -> {
            o.delete();
            SpaceDeletedEvent.with(o.id).emit();
        });
        return count;
    }

    // @COMMAND
    public Space createSpace(Space s) throws RuntimeException {

        Space.findByNumberOptional(s.number)
            .ifPresent((o)->{ throw new SpaceExistsException(); });

        Space spc = new Space();
        spc.number = s.number;
        spc.residentApprovedByOwner = false;
        spc.persist();
        SpaceCreatedEvent.with(spc.id, spc.number).emit();;         
        return spc;
    }

    // @EVENT
    public void handleOwnerDeleted(OwnerDeletedEvent e) {
        try {
            Optional<Space> o_spc = Space.find("ownerId", e.ownerId).firstResultOptional();
            o_spc.orElseThrow(SpaceDoesNotExistException::new);

            Space spc = o_spc.get();
            Space spc_orig = o_spc.get();

            spc.ownerId = null;
            spc.residentId = null;
            spc.residentApprovedByOwner = false;
            spc.update();

            SpaceOwnerRemovedEvent.with(spc.id).emit();

            // Only emit if resident was orginally set
            SpaceResidentRemovedEvent.with(spc.id).emitIf(spc_orig.residentId != null);

            // Only emit if approved previously; With new owner the resident must be re-associated and re-approved
            SpaceResidentApprovalRemovedEvent.with(spc.id).emitIf(spc_orig.residentApprovedByOwner);

        } catch (Exception ex) {
            log.error("Error processing owner-deleted", ex);   
        }
    }

    // @EVENT
    public void handleOwnerCreated(OwnerCreatedEvent e) {
        try {
            log.info("Owner created for space {}", e.spaceId);
            Optional<Space> o_spc = Space.findByIdOptional(new ObjectId(e.spaceId));
            o_spc.orElseThrow(SpaceDoesNotExistException::new);

            // Change owner on existing space
            Space spc_orig = o_spc.get();
            Space spc = o_spc.get();

            spc.ownerId = e.ownerId;
            // When new Owner is created the Space's resident must be set and approved again
            spc.residentId = null;
            spc.residentApprovedByOwner = false;

            spc.update();

            SpaceOwnerRemovedEvent.with(spc.id).emitIf(spc_orig.ownerId != null);
            SpaceOwnerSetEvent.with(spc.id, spc.ownerId);

            // Only emit if approved previously; With new owner the resident must be re-associated and re-approved
            SpaceResidentRemovedEvent.with(spc.id).emitIf(spc_orig.residentId != null);
            SpaceResidentApprovalRemovedEvent.with(spc.id).emitIf(spc_orig.residentApprovedByOwner);

        } catch (Exception ex) {
            log.error("Error processing owner-created", ex);   
        }
    }

    // @Incoming("resident-created-received")
    // public void handleResidentCreated(@NonNull JsonObject oc_evt_in) {
    //     JsonObject oc_evt = oc_evt_in.getJsonObject("map");

    //     try {
    //         // Event contains the residentId and ownerId only
    //         log.info("oc_evt: ", oc_evt);
            
    //         Optional<Space> o_spc = Space.find("id", oc_evt.getString("spaceId")).firstResultOptional();
    //         if((o_spc.isPresent())) {
    //             // Change owner on existing space
    //             Space spc = o_spc.get();
    //             Space spc_orig = Space.from(o_spc.get());

    //             spc.residentId = oc_evt.getString("id");
    //             spc.residentApprovedByOwner = false;
    //             spc.update();

    //             event.emitIf(spc_orig.residentId != null, "space-resident-changed", spc_orig, residentChangedEventFields);
    //             event.emitIf(spc_orig.residentApprovedByOwner != null && spc_orig.residentApprovedByOwner == true, 
    //                 "space-resident-approval-by-owner-removed", spc_orig, residentApprovalRemovedEventFields);

    //              log.info("Updated space residenrt {}", spc);

    //         } else {
    //             throw new Exception("Space doesn't exist");
    //         }
    //     } catch (Exception e) {
    //         log.error("Error processing owner-created", e);            
    //     }
    // }

    // @Incoming("resident-deleted-received")
    // public void handleResidentDeleted(@NonNull JsonObject o_evt_in) {
    //     JsonObject o_evt = o_evt_in.getJsonObject("map");

    //     try {
    //         Optional<Space> o_spc = Space.find("owner", o_evt.getString("id")).firstResultOptional();
    //         if((o_spc.isPresent())) {
    //             // Remove owner on existing space
    //             Space spc_orig = Space.from(o_spc.get());
    //             Space spc = o_spc.get();
    //             spc.residentId = null;
    //             spc.residentApprovedByOwner = false;
    //             spc.update();

    //             event.emit("space-resident-removed", spc_orig, residentRemovedEventFields);
    //             event.emit("space-resident-approval-by-owner-removed", spc_orig, residentApprovalRemovedEventFields);
    //             log.info("Updated space owner {}", spc);

    //         } else {
    //             throw new Exception("Space doesn't exist");
    //         }
    //     } catch (Exception e) {
    //         log.error("Error processing owner-deleted", e);
            
    //     }
    // }

    // @Incoming("owner-approved-resident")
    // public void handleResidentApprovelReceived(@NonNull JsonObject o_evt_in) {
    //     JsonObject o_evt = o_evt_in.getJsonObject("map");
    //     try {
    //         Optional<Space> o_spc = Space.find("owner", o_evt.getString("id")).firstResultOptional();
    //         if((o_spc.isPresent())) {
    //             Space spc = o_spc.get();

    //             if(spc.residentId != null) {
    //                 spc.residentApprovedByOwner = true;
    //                 spc.update();
    //                 event.emitId("space-resident-approved-by-owner", spc);
    //             } else {
    //                 throw new Exception("Resident not set");
    //             }
    //        } else {
    //             throw new Exception("Space doesn't exist");
    //         }
    //     } catch (Exception e) {
    //         log.error("Error processing owner-deleted", e);
            
    //     }
    //  }

    //  @Incoming("owner-disapproved-resident")
    //  public void handleResidentDisapprovalReceived(@NonNull JsonObject o_evt_in) {
    //      JsonObject o_evt = o_evt_in.getJsonObject("map");
    //      try {
    //          Optional<Space> o_spc = Space.find("owner", o_evt.getString("id")).firstResultOptional();
    //          if((o_spc.isPresent())) {
    //              Space spc = o_spc.get();
 
    //              if(spc.residentId != null) {
    //                  spc.residentApprovedByOwner = false;
    //                  spc.update();
    //                  event.emitId("space-resident-approved-by-owner", spc);
    //              } else {
    //                  throw new Exception("Resident not set");
    //              }
    //         } else {
    //              throw new Exception("Space doesn't exist");
    //          }
    //      } catch (Exception e) {
    //          log.error("Error processing owner-deleted", e);
             
    //      }
    //   }

    // @Outgoing("space-owner-changed")
    // public Flowable<JsonObject> createOwnerChangedCreatedStream() {
    //     return event.fromChannelName("space-owner-changed");
    // } 
    
    // @Outgoing("space-resident-changed")
    // public Flowable<JsonObject> createResidentChangedCreatedStream() {
    //     return event.fromChannelName("space-owner-changed");
    // }

    // @Outgoing("space-resident-approved-by-owner")
    // public Flowable<JsonObject> createResidentAprrovedStream() {
    //     return event.fromChannelName("space-resident-approved-by-owner");
    // }

}
