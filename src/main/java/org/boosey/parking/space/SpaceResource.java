package org.boosey.parking.space;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.boosey.parking.space.SpaceService.SpaceDoesNotExistException;
import org.boosey.parking.space.SpaceService.SpaceExistsException;
import lombok.Data;
import lombok.NonNull;

@Data 
@Path("/space")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@ApplicationScoped
public class SpaceResource {

    @Inject 
    SpaceService service;

    @POST
    public Response create(@NonNull Space space) {
        try {      
            return Response.status(Status.CREATED).entity(service.createSpace(space)).build();

        } catch (SpaceExistsException e) {
            return Response.status(409).build();

        } catch (Exception e) {
            return Response.status(500).build();
        }
    }

    @GET
    public Response listAll(){
        try {
            return Response.ok(service.listSpaces()).build(); 
                       
        } catch (Exception e) {
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("/{id}")
    public Response getSpaceById(@PathParam("id") String id) throws RuntimeException {
        try {
            return Response.ok(service.getSpaceById(id)).build();

        } catch(SpaceDoesNotExistException e) {
            return Response.status(Status.NOT_FOUND).build();

        } catch(IllegalArgumentException e) {
            return Response.status(Status.BAD_REQUEST).build();

        } catch (RuntimeException e) {
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    public Response deleteAll() {
        try {
            return Response.ok(service.deleteAllSpaces()).build();   

        } catch (Exception e) {
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}